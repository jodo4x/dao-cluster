"use strict";

var cong = [
    "$httpProvider",
    "$routeProvider",
    "$tooltipProvider",
    function( $httpProvider, $routeProvider, $tooltipProvider ) {
        $httpProvider.interceptors.push( "httpInt" )

        $routeProvider.otherwise({
            redirectTo: "/"
        })

        $tooltipProvider.options({
            appendToBody: true
        })
    }
]

module.exports = cong

