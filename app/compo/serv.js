"use strict";

var API = "http://192.168.1.92:7433"

// simple domain pattern
var domainPattern = /(^(?:\w+\.)+(?:[\w\/]+)$)/

// simple ipv4 pattern
var ipv4Pattern = /(^(?:\d+\.){3}(?:\d+)(?::[\d]{2,4})?$)/

var uPattern = /([\w]{2,20})/
var pPattern = /([\w]{6,30})/

var httpInt = [
    "$rootScope",
    "$q",
    "$cookies",
    function( $rootScope, $q, $cookies ) {
        function _request( data ) {
            data.headers = data.headers || {}

            if ( $cookies.get( "token" ) ) {
                data.headers[ "Authorization" ] = "Basic " + $cookies.get( "token" )
            }

            return data
        }

        function _responseError( rejection ) {
            return $q.reject( rejection )
        }

        return {
            request       : _request,
            responseError : _responseError
        }
    }
]

var promise = [
    "$q",
    "$http",
    "API",
    function( $q, $http, API ) {
        function _expect( url, options ) {
            var req = {
                method : "GET",
                url    : API + url,
                params : null,
                data   : null
            }

            options && angular.extend( req, options )

            var defer = $q.defer()

            $http( req )
            .success(function( res ) {
                defer.resolve( res )
            })
            .error(function( err ) {
                defer.reject( err )
            })

            return defer.promise
        }

        return {
            expect: _expect
        }
    }
]

var objFactory = [
    function() {
        function _keys( obj ) {
            var keys = Object.keys( obj )

            var tmp
            keys.forEach(function( key ) {

                // `angular.isObject([])` returns `true`
                if ( angular.isArray( obj[ key ] ) ) {
                    tmp = []
                    tmp = angular.copy( obj[ key ] )
                    obj[ angular.lowercase( key ) ] = tmp

                    tmp.forEach(function( item ) {
                        _keys( item )
                    })
                } else if ( angular.isObject( obj[ key ] ) ) {
                    tmp = {}
                    tmp = angular.copy( obj[ key ] )
                    obj[ angular.lowercase( key ) ] = tmp

                    _keys( tmp )
                } else {
                    obj[ angular.lowercase( key ) ] = obj[ key ]
                }

                delete obj[ key ]
            })

            keys = null
        }

        return {
            keys: _keys
        }
    }
]

var alrtFactory = [
    "$rootScope",
    function( $rootScope ) {
        var Alrt = function Alrt( data ) {
            var p
            for ( p in data ) {
                if ( data.hasOwnProperty( p ) ) {
                    this[ p ] = data[ p ]
                }
            }
        }

        Alrt.prototype.dismiss = function() {
            // alrt实例已经没有引用,可以安全的移除
            $rootScope.alrt = null
        }

        function show( data ) {
            $rootScope.alrt = new Alrt( data )
            console.log( $rootScope )
        }

        return {
            show: show
        }
    }
]

exports.API = API
exports.domainPattern = domainPattern
exports.ipv4Pattern = ipv4Pattern
exports.uPattern = uPattern
exports.pPattern = pPattern
exports.httpInt = httpInt
exports.promise = promise
exports.objFactory = objFactory
exports.alrtFactory = alrtFactory

