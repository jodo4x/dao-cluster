"use strict";

var cong = [
    "$routeProvider",
    function( $routeProvider ) {
        $routeProvider
        .when("/", {
            templateUrl: "tpl__auth.html",
            controller: "authCtrl as ctrl"
        })
    }
]

module.exports = cong

