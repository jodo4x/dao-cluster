"use strict";

var angular = require( "angular" )
var cong = require( "./cong" )
var serv = require( "./serv" )
var ctrl = require( "./ctrl" )

var auth = "app.auth"

angular
.module( auth, [] )
.config ( cong )
.factory( "authFactory", serv.authFactory )
.controller( "authCtrl", ctrl.authCtrl )

module.exports = auth

