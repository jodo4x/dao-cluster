"use strict";

var authCtrl = [
    "$rootScope",
    "$location",
    "$cookies",
    "$base64",
    "uPattern",
    "pPattern",
    "authFactory",
    "alrtFactory",
    function( $rootScope, $location, $cookies, $base64,
            uPattern, pPattern, authFactory, alrtFactory ) {
        var _this = this

        if ( $cookies.get( "token" ) ) {
            // FIXME
            // if user has been logged in, take him to another url.
            // $location.path( "/installation/add" )
        }

        this.cred = {
            username: "",
            password: ""
        }

        this.auth = function() {
            var token, d, exp
            var cred = _this.cred

            if ( uPattern.test( cred.username )
                    && pPattern.test( cred.password ) ) {

                // FIXME
                // Basic Authorization
                d = new Date
                d.setTime( d.getTime() + 1*24*60*60*1000 )
                exp = d.toUTCString()
                token = $base64.encode( cred.username + ":" + cred.password )
                $cookies.put( "token", token, { expires: exp } )

                authFactory.auth( _this.cred )
                .then(function ( res ) {
                    $location.path( "/installation/nodes" ).replace()
                }, function( err ) {

                    // FIXME
                    // auth() -> {Basic Authorization} -> /api/auth
                    // auth() -> /api/auth
                    // these codes will never execute based on "{Basic Authorization}"
                    $cookies.remove( "token" )

                    alrtFactory.show({
                        status: "danger",
                        message: "foobar"
                    })
                })
            } else {
                alrtFactory.show({
                    status: "danger",
                    message: "username or password are not matched, please try again."
                })
            }
        }
    }
]

exports.authCtrl = authCtrl

