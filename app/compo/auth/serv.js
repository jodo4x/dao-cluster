"use strict";

var authFactory = [
    "promise",
    function( promise ) {
        function _auth( cred ) {
            var url = "/auth"
            var options = {
                method: "POST",
                data: cred
            }

            return promise.expect( url, options )
        }

        return {
            auth: _auth
        }
    }
]

exports.authFactory = authFactory

