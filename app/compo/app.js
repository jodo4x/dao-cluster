"use strict";

var route = require( "angular-route" )
var cookies = require( "angular-cookies" )
var base64 = require( "angular-base64" ) || "base64" // FIXME: `require( "angular-base64" ) === undefined`
var uilb = require( "angular-loading-bar" )
var uibs = require( "angular-ui-bootstrap" )
var navbar = require( "./navbar/navbar" )
var auth = require( "./auth/auth" )
var node = require( "./node/node" )
var cluster = require( "./cluster/cluster" )

var cong = require( "./cong" )
var run = require( "./run" )
var serv = require( "./serv" )

var app = "app"

angular
.module( app, [ route, cookies, base64, uilb, uibs, navbar, auth, node, cluster ] )
.config( cong )
.run( run )
.value( "API", serv.API )
.value( "ipv4Pattern", serv.ipv4Pattern )
.value( "uPattern", serv.uPattern )
.value( "pPattern", serv.pPattern )
.constant( "NODES", serv.NODES )
.constant( "SERVICES", serv.SERVICES )
.value( "domainPattern", serv.domainPattern )
.factory( "httpInt", serv.httpInt )
.factory( "promise", serv.promise )
.factory( "objFactory", serv.objFactory )
.factory( "alrtFactory", serv.alrtFactory )

module.exports = app

