"use strict";

var ctrl = [
    "$interval",
    "clusterFactory",
    "nodes",
    function( $interval, clusterFactory, nodes ) {
        this.nodes = nodes

        this.nodes.forEach(function( item ) {
            var current = {
                // - "deploying"
                // - "deploying_error"
                // - "success"
                result: "",
                _interFn: null,
                caller: function() {
                    this._interFn = $interval(function() {
                        if ( this.result === "success" || this.result === "deploying_error" ) {
                            $interval.cancel( this._interFn )
                            return
                        }

                        var _this = this
                        clusterFactory.getStatus( { index: item.index } )
                        .then(function( res ) {
                            item.deployed = res.data
                            res.data && ( _this.result = res.data )
                        }, function( err ) {
                            item.deployed = res.data
                            err.data && ( _this.result = err.data )
                        })
                    }.bind( this ), 2000)
                }
            }

            current.caller()
        })
    }
]

module.exports = ctrl

