"use strict";

var ctrl = [
    "$rootScope",
    "$location",
    "clusterFactory",
    "alrtFactory",
    function( $rootScope, $location, clusterFactory, alrtFactory ) {
        var _this = this

        this.tab = 0

        // initial type
        this.type = this.tab

        this.cluster = {
            // common
            registry : "",
            
            // dao paas
            domain   : "",
            influxdb : "",
            graylog  : "",

            // dao ci
            host     : "",
            url      : ""
        }

        this.setup = function( cluster ) {
            var pCluster

            if ( cluster.domain ) {
                pCluster = {
                    "env_dao_docker_registry"              : cluster.registry,
                    "env_dao_app_domain"                   : cluster.domain,
                    "env_dao_cadvisor_storage_driver_host" : cluster.influxdb,
                    "env_dao_node_graylog_server_address"  : cluster.graylog
                }
            }

            if ( cluster.url ) {
                pCluster = {
                    "env_dao_docker_registry"              : cluster.registry,
                    "dao_ci_registry_host"                 : cluster.host,
                    "dao_ci_url"                           : cluster.url
                }
            }

            clusterFactory.setup( pCluster )
            .then(function ( res ) {
                $location.path( "/installation/mapping" )
            })
        }
    }
]

module.exports = ctrl

