"use strict";

var ctrl = [
    "$rootScope",
    "$location",
    "$timeout",
    "$q",
    "mapService",
    "clusterFactory",
    "alrtFactory",
    "cluster",
    function( $rootScope, $location, $timeout, $q, mapService,
            clusterFactory, alrtFactory, cluster ) {
        var _this = this

        var _slcN = [], _slcS = [], _rqd = 0

        function _deploy() {
            clusterFactory.deploy()
            .then(function( res ) {
                if ( res.status === 200 ) {
                    $location.path( "/installation/installing" )
                }
            }, function( err ) {
                throw err
            })
        }

        this.tab = 0

        this.nodes = cluster.nodes
        this.servs = cluster.servs

        // NOTE: `cluster.servs` is a promised object
        $timeout(function() {
            var defer = $q.defer()
            defer.resolve( _this.servs )
            return defer.promise
        })
        .then(function( res ) {
            res.forEach(function( item ) {
                if ( item.required ) {
                    _rqd += 1
                }
            })
        })

        // ```js
        // [
        // { node_index: 1, service_index: 2 }
        // ]
        // ```
        this.maps = []

        this.mapping = function( node, serv ) {
            var nIdx = node.index,
                sIdx = serv.index
            
            var map

            // [x] 当前节点已绑定
            if ( _slcN.indexOf( node ) !== -1 ) {
                alrtFactory.show({
                    status: "warning",
                    message: "this node was binded, please remove the service first then change bind."
                })

            // [ ] 当前节点未绑定
            } else {

                // [x] 当前服务已绑定
                if ( _slcS.indexOf( serv ) !== -1 ) {

                    // [ ] 当前服务可扩展
                    if ( serv.scalable ) {
                        map = mapService.init( nIdx, sIdx )
                        _this.maps.push( map )
                        _slcN.push( node )
                        node.map = map
                        node.binded = sIdx
                        if ( serv.binded ) {
                            serv.binded.push( nIdx )
                        } else {
                            ( serv.binded = [] ) && serv.binded.push( nIdx ) // serv is scalable
                        }

                    // [x] 当前服务不可扩展
                    } else {
                        alrtFactory.show({
                            status: "warning",
                            message: "this server is unscalable and has been binded."
                        })
                    }

                // [ ]  当前服务未绑定
                } else {
                    map = mapService.init( nIdx, sIdx )
                    _this.maps.push( map )
                    _slcN.push( node )
                    _slcS.push( serv )
                    node.map = map
                    node.binded = sIdx
                    if ( serv.scalable ) {
                        ( serv.binded = [] ) && serv.binded.push( nIdx ) // serv is scalable
                    } else {
                        serv.binded = nIdx
                    }
                }
            }
        }

        this.remove = function( node, serv ) {
            var nMap = mapService.init( node.index, serv.index )

            try {
                _this.maps.forEach(function( item, idx ) {
                    if ( angular.equals( item, nMap ) ) {
                        throw idx
                    }
                })
            } catch( e ) {
                _this.maps.splice( e, 1 )
            }

            _slcN.splice( _slcN.indexOf( node ), 1 )

            node.binded = ""

            if ( serv.scalable ) {

                // 先判断服务绑定的节点数
                // 如果只有一个节点,则可以安全的从`_slcS`中移除
                if ( serv.binded.length === 1 ) {
                    _slcS.splice( _slcS.indexOf( serv ), 1 )
                }
                serv.binded.splice( serv.binded.indexOf( node.index ), 1 )
            } else {
                serv.binded = ""
                _slcS.splice( _slcS.indexOf( serv ), 1 )
            }
        }

        this.save = function( maps ) {

            // TODO
            // remove same `map`
            /*
            var keys = [], strKeys
            maps.forEach(function( item ) {
                strKeys = JSON.stringify( Object.keys( item ).sort() )
                keys.push( strKeys )
            })

            var key
            keys.forEach(function( item ) {
                if ( item === key ) {
                    keys.splice( keys.indexOf( item ), 1 )
                }

                key = item
            })
            */

            var slcS = _slcS.filter(function( item ) {
                return item.required
            })

            if ( slcS.length === _rqd ) {
                clusterFactory.mapping( maps )
                .then(function( res ) {
                    if ( res.status === 200 ) {
                        _deploy()
                    }
                })
            } else {
                alrtFactory.show({
                    status: "warning",
                    message: "some requied servs were not binded yet."
                })
            }
        }
    }
]

module.exports = ctrl

