"use strict";

var servList = [
    "$templateCache",
    "$compile",
    function( $templateCache, $compile ) {
        return {
            link: function( scope, el, attrs ) {
                var tpl = $templateCache.get( attrs.templateUrl )
                var rsltEl = $compile( tpl )( scope )

                el.replaceWith( rsltEl )
            }
        }
    }
]

var setupForm = [
    "$templateCache",
    "$compile",
    "domainPattern",
    "ipv4Pattern",
    "alrtFactory",
    function( $templateCache, $compile, domainPattern, ipv4Pattern, alrtFactory ) {
        var linkFn = function linkFn( scope, el, attrs ) {
            var tpl = $templateCache.get( attrs.templateUrl )
            var rsltEl = $compile( tpl )( scope )

            el.replaceWith( rsltEl )

            var doSetup = function doSetup() {
                var cluster = scope.cluster

                // make sure field `domain` or `url` is not blank.
                if ( cluster.domain || cluster.url ) {

                    // if `domain` is true, it is dao paas.
                    if ( cluster.domain ) {
                        if ( !domainPattern.test( cluster.domain ) ) {
                            alrtFactory.show({
                                status: "warning",
                                message: "domain is incorrect."
                            })
                            return
                        } else if ( !ipv4Pattern.test( cluster.influxdb ) ) {
                            alrtFactory.show({
                                status: "warning",
                                message: "influxdb address is incorrect."
                            })
                            return
                        }

                    // if `url` is true, it is dao ci.
                    } else {
                        // TODO pattern test of dao ci fields.
                    }

                    scope.setup( { cluster: cluster } )
                } else {
                    alrtFactory.show({
                        status: "warning",
                        message: "some fields are required."
                    })
                }
            }

            scope.doSetup = doSetup
        }

        return {
            scope: {
                cluster: "=",
                setup: "&"
            },
            link: linkFn
        }
    }
]

exports.servList = servList
exports.setupForm = setupForm

