"use strict";

var deploy = [
    "DEPLOY_STATUS",
    function( DEPLOY_STATUS ) {
        return function( input ) {
            return DEPLOY_STATUS[ input ]
        }
    }
]

var text = [
    function() {
        return function( input ) {
            if ( input === "deploying" )
                return "info"
            if ( input === "deploying_error" )
                return "danger"
            if ( input === "success" )
                return "success"
        }
    }
]

var glyphicon = [
    function() {
        return function( input ) {
            if ( input === "deploying" )
                return "time"
            if ( input === "deploying_error" )
                return "minus"
            if ( input === "success" )
                return "ok"
        }
    }
]

exports.deploy = deploy
exports.text = text
exports.glyphicon = glyphicon
