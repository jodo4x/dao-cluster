"use strict";

var DEPLOY_STATUS = {
    "deploying"       : "\u6b63\u5728\u90e8\u7f72",
    "deploying_error" : "\u90e8\u7f72\u5931\u8d25",
    "success"         : "\u90e8\u7f72\u6210\u529f"
}

var Map = function Map( nIdx, sIdx ) {
    // FIXME
    // ```js
    // this.node_index = nIdx
    // this.service_index = sIdx
    // ```
    this.nodeindex = nIdx
    this.serviceindex = sIdx
}

var mapService = [
    function() {
        this.init = function( n, s ) {
            return new Map( n, s )
        }
    }
]

var clusterFactory = [
    "promise",
    "objFactory",
    function( promise, objFactory ) {
        function _setup( cluster ) {
            var url = "/confirm_daopaas_info"
            var options = {
                method: "POST",
                data: cluster
            }

            return promise.expect( url, options )
        }

        function _getServs() {
            var servs
            var url = "/get_daopaas_services"

            return promise.expect( url )
            .then(function( res ) {
                servs = res.data

                if ( angular.isArray( servs )
                        && servs.length ) {
                    servs.forEach(function( item ) {
                        objFactory.keys( item )
                    })
                }

                return servs
            })
        }

        function _mapping( map ) {
            var url = "/confirm_daopaas_mapping"
            var options = {
                method: "POST",
                data: map
            }

            return promise.expect( url, options )
        }

        function _deploy() {
            var url = "/deploy_daopaas"
            var options = {
                method: "POST"
            }

            return promise.expect( url, options )
        }

        function _getStatus( params ) {
            var url = "/deploy_status"
            var options = {
                params: params
            }

            return promise.expect( url, options )
        }

        return {
            setup     : _setup,
            getServs  : _getServs,
            mapping   : _mapping,
            deploy    : _deploy,
            getStatus : _getStatus
        }
    }
]

exports.DEPLOY_STATUS = DEPLOY_STATUS
exports.mapService = mapService
exports.clusterFactory = clusterFactory

