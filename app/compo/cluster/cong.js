"use strict";

var cong = [
    "$routeProvider",
    function( $routeProvider ) {
        $routeProvider
        .when("/installation/setup", {
            templateUrl: "compo/cluster/tpl/setup.html",
            controller: "clusterSetupCtrl as ctrl"
        })
        .when("/installation/mapping", {
            templateUrl: "compo/cluster/tpl/mapping.html",
            controller: "clusterMappingCtrl as ctrl",
            resolve: {
                cluster: [
                    "$q",
                    "nodeFactory",
                    "clusterFactory",
                    function( $q, nodeFactory, clusterFactory ) {
                        var promises = {
                            nodes: nodeFactory.query(),
                            servs: clusterFactory.getServs()
                        }

                        return $q.all( promises )
                    }
                ]
            }
        })
        .when("/installation/installing", {
            templateUrl: "compo/cluster/tpl/install.html",
            controller: "clusterInstallingCtrl as ctrl",
            resolve: {
                nodes: ["nodeFactory", function( nodeFactory ) {
                    return nodeFactory.query()
                }]
            }
        })
    }
]

module.exports = cong
