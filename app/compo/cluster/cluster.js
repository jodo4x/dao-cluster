"use strict";

var angular = require( "angular" )
var cong = require( "./cong" )
var serv = require( "./serv" )
var setupCtrl = require( "./ctrl/setup" )
var mappingCtrl = require( "./ctrl/mapping" )
var installingCtrl = require( "./ctrl/install" )
var dir = require( "./dir" )
var fltr = require( "./fltr" )

var cluster = "app.cluster"

angular
.module( cluster, [] )
.config( cong )
.constant( "DEPLOY_STATUS", serv.DEPLOY_STATUS )
.service( "mapService", serv.mapService )
.factory( "clusterFactory", serv.clusterFactory )
.controller( "clusterSetupCtrl", setupCtrl )
.controller( "clusterMappingCtrl", mappingCtrl )
.controller( "clusterInstallingCtrl", installingCtrl )
.directive( "serviceList", dir.servList )
.directive( "setupForm", dir.setupForm )
.filter( "deploy", fltr.deploy )
.filter( "text", fltr.text )

module.exports = cluster

