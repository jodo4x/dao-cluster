"use strict";

var angular = require( "angular" )
var ctrl = require( "./ctrl" )
var dir = require( "./dir" )

var navbar = "app.navbar"

angular
.module( navbar, [] )
.controller( "navbarCtrl", ctrl.navbarCtrl )
.directive( "navbar", dir.navbar )

module.exports = navbar

