"use strict";

var navbar = [
    function() {
        return {
            scope: {},
            replace: true,
            templateUrl: "tpl__navbar.html",
            controller: "navbarCtrl as ctrl",
            bindToController: true
        }
    }
]

exports.navbar = navbar

