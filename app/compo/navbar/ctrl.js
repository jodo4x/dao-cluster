"use strict";

var navbarCtrl = [
    "$location",
    "$cookies",
    function( $location, $cookies ) {
        var _this = this

        this.isLoggedIn = function() {
            return $cookies.get( "token" ) ? 1 : 0
        }

        this.logout = function() {
            $cookies.remove( "token" )
            _this.isLoggedIn()
            $location.path( "/" ).replace()
        }
    }
]

exports.navbarCtrl = navbarCtrl

