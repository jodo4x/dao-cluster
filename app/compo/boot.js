"use strict";

var angular = require( "angular" )
var app = require( "./app" )

var boot = function boot() {
    angular.element( document )
    .ready(function() {
        angular.bootstrap( document, [ app ] )
    })
}

module.exports = boot

