"use strict";

var NODE_STATUS = {
    "999": "\u521d\u59cb\u5316",
    "200": "\u53ef\u8fde\u63a5",
    "404": "\u65e0\u7f51\u7edc"
}

var Node = function Node( index, ip ) {
    this.index  = index || 1
    this.ip     = ip || ""
    this.status = 999
}

Node.prototype.foo = function() {
    console.log( "call foo() will print my index, %s", this.index )
}

var nodeService = [
    function() {
        this.init = function( index, ip ) {
            return new Node( index, ip )
        }
    }
]

var nodeFactory = [
    "promise",
    "objFactory",
    function( promise, objFactory ) {
        function _ping( node ) {
            var url = "/ping"
            var options = {
                method: "POST",
                data: node
            }

            return promise.expect( url, options )
        }

        function _saveAllNodes( nodes ) {
            var url = "/save_all_nodes"
            var options = {
                method: "POST",
                data: nodes
            }

            return promise.expect( url, options )
        }

        function _login( node ) {
            var url = "/auth_credential"
            var options = {
                method: "POST",
                data: node
            }

            return promise.expect( url, options )
        }

        function _getSysDetails( node ) {
            var url = "/get_sys_details"
            var options = {
                params: node
            }

            return promise.expect( url, options )
        }

        function _query() {
            var url = "/get_all_nodes"

            var nodes
            return promise.expect( url )
            .then(function( res ) {
                nodes = res.data

                if ( angular.isArray( nodes )
                        && nodes.length ) {
                    nodes.forEach(function( item ) {
                        objFactory.keys( item )
                    })
                }

                return nodes
            })
        }

        return {
            ping          : _ping,
            saveAllNodes  : _saveAllNodes,
            login         : _login,
            getSysDetails : _getSysDetails,
            query         : _query
        }
    }
]

exports.NODE_STATUS = NODE_STATUS
exports.nodeService = nodeService
exports.nodeFactory = nodeFactory

