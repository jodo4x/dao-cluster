"use strict";

var angular = require( "angular" )
var cong = require( "./cong" )
var serv = require( "./serv" )
var fltr = require( "./fltr" )
var addCtrl = require( "./ctrl/add" )
var loginCtrl = require( "./ctrl/login" )
var summaryCtrl = require( "./ctrl/index" )

var node = "app.node"

angular
.module( node, [] )
.config( cong )
.constant( "NODE_STATUS", serv.NODE_STATUS )
.service( "nodeService", serv.nodeService )
.factory( "nodeFactory", serv.nodeFactory )
.controller( "nodeAddCtrl", addCtrl )
.controller( "nodeLoginCtrl", loginCtrl )
.controller( "nodeSummaryCtrl", summaryCtrl )
.filter( "status", fltr.status )
.filter( "glyphicon", fltr.glyphicon )
.filter( "panel", fltr.panel )

module.exports = node

