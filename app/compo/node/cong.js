"use strict";

module.exports = [
    "$routeProvider",
    function( $routeProvider ) {
        var _resolve = {
            nodes: [ "nodeFactory", function( nodeFactory ) {
                return nodeFactory.query()
            }]
        }

        var routes = {
            "/installation/nodes": {
                templateUrl: "compo/node/tpl/add.html",
                controller: "nodeAddCtrl as ctrl",
                resolve: _resolve
            },
            "/installation/login": {
                templateUrl: "compo/node/tpl/login.html",
                controller: "nodeLoginCtrl as ctrl",
                resolve: _resolve
            },
            "/installation/summary": {
                templateUrl: "compo/node/tpl/index.html",
                controller: "nodeSummaryCtrl as ctrl",
                resolve: _resolve
            }
        }

        angular.forEach(routes, function( route, path ) {
            $routeProvider.when( path, route )
        })
    }
]

