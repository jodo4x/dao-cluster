"use strict";

var ctrl = [
    "$rootScope",
    "$location",
    "$q",
    "ipv4Pattern",
    "nodeService",
    "nodeFactory",
    "alrtFactory",
    "nodes",
    function( $rootScope, $location, $q, ipv4Pattern,
            nodeService, nodeFactory, alrtFactory, nodes ) {
        var _this = this

        function _ping( node ) {
            var pNode = {}

            if ( ipv4Pattern.test( node.ip ) ) {
                pNode.ip = node.ip

                return nodeFactory.ping( pNode )
                .then(function( res ) {
                    if ( res.status ) {
                        node.status = res.status
                    }

                    // return `res` for further operation
                    return res
                }, function( err ) {
                    if ( err.status ) {
                        node.status = err.status
                    }

                    // return `err` for further operation
                    return err
                })
            } else {
                alrtFactory.show({
                    status: "danger",
                    message: "ip is not match the correct format."
                })
            }
        }

        function _saveAllNodes( nodes ) {
            if ( nodes
                    && angular.isArray( nodes )
                    && nodes.length ) {
                nodeFactory.saveAllNodes( nodes )
                .then(function( res ) {
                    if ( res.status === 200 ) {
                        $location.path( "/installation/login" )
                    }
                }, function( err ) {
                    throw err
                })
            } else {
                throw "nodes is unavialable."
            }
        }

        this.nodes = nodes

        this.add = function() {
            var nodes = _this.nodes,
                len = nodes.length

            var node = nodeService.init( ++len )
            _this.nodes.push( node )
            _this.canSave = false
        }

        this.edit = function( node ) {
            node.status = 999
            _this.canSave = false
        }

        this.remove = function( node ) {
            var nodes = _this.nodes
            var idx = node.index

            nodes.splice( --idx, 1 )
            nodes.forEach(function( item, i ) {
                item.index = ++i
            })
        }

        this.ping = function( node ) {
            _ping( node )
        }

        this.pingAll = function( nodes ) {
            var promises = []

            nodes.forEach(function( node ) {
                promises.push( _ping( node ) )
            })

            $q.all( promises )
            .then(function( res ) {
                var exception = {}

                try {
                    res.forEach(function( item ) {
                         if ( item[ "status" ] === 404 ) {
                             exception.passed = false
                             throw exception
                         }
                    })

                    exception.passed = true
                    throw exception
                } catch( err ) {
                    if ( !err.passed ) {
                        alrtFactory.show({
                            status: "danger",
                            message: "some nodes are not reachable."
                        })
                    }

                    _this.canSave = err.passed
                }

                // short way
                
                /*
                // `arr.some()`
                _this.canSave = !res.some(function( item ) {
                    return item[ "status" ] === 404
                })
                */
                
                // `arr.every()`
                /*
                _this.canSave = res.every(function( item ) {
                    return item[ "status" ] === 200
                })
                */
            })
        }

        this.canSave = false

        this.save = function( nodes ) {
            if ( _this.canSave ) {
                _saveAllNodes( nodes )
            }
        }
    }
]

module.exports = ctrl

