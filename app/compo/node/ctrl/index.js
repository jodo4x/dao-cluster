"use strict";

var ctrl = [
    "$location",
    "$q",
    "nodeFactory",
    "nodes",
    function( $location, $q, nodeFactory, nodes ) {
        this.nodes = nodes

        this.next = function() {
            $location.path( "/installation/setup" )
        }
    }
]

module.exports = ctrl

