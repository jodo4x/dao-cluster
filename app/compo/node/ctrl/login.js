"use strict";

var ctrl = [
    "$location",
    "$q",
    "uPattern",
    "pPattern",
    "nodeFactory",
    "alrtFactory",
    "nodes",
    function( $location, $q, uPattern, pPattern, 
            nodeFactory, alrtFactory, nodes ) {
        var _this = this

        function _login( node, cred ) {
            var pNode

            node.username = cred.username
            node.password = cred.password
            pNode = {
                ip       : node.ip,
                username : cred.username,
                password : cred.password
            }

            return nodeFactory.login( node )
            .then(function( res ) {
                return nodeFactory.getSysDetails( pNode )
            }, function( err ) {
                return err
            })
            .then(function( res ) {
                var data

                if ( res.status === 200 ) {
                    data = res.data

                    node.authed = true
                    node.node_details = {
                        Hardware: data[ "Hardware" ],
                        System_Info: data[ "System_Info" ]
                    }
                } else {
                    node.authed = false
                }

                return node
            })
        }

        this.nodes = nodes
        this.cred = {
            username: "",
            password: ""
        }

        this.login = function( cred ) {
            var promises = []

            if ( !uPattern.test( cred.username ) ) {
                console.log( uPattern, cred )
                alrtFactory.show({
                    status: "warning",
                    message: "username is incorrect."
                })
            } else if ( !pPattern.test( cred.password ) ) {
                console.log( pPattern, cred )
                alrtFactory.show({
                    status: "warning",
                    message: "password is incorrect."
                })
            } else {
                _this.nodes.forEach(function( node ) {
                    promises.push( _login( node, cred ) )
                })

                $q.all( promises )
                .then(function( res ) {
                    var isAllAuthed = res.every(function( item ) {
                        return item.authed
                    })

                    if ( isAllAuthed ) {
                        nodeFactory.saveAllNodes( _this.nodes )
                        .then(function( res ) {
                            $location.path("/installation/summary")
                        }, function( err ) {
                            throw err
                        })
                    } else {

                        // TODO
                        // tell me which node was unauthorized
                        var unauthes = []
                        res.forEach(function( item ) {
                            if ( !item.authed ) {
                                unauthes.push( item.ip )
                            }
                        })

                        alrtFactory.show({
                            status: "danger",
                            message: "some nodes were unauthorized, " + unauthes.join( " , " )
                        })
                    }
                })
            }
        }
    }
]

module.exports = ctrl

