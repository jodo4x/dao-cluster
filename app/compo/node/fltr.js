"use strict";

var status = [
    "NODE_STATUS",
    function( NODE_STATUS ) {
        return function( input ) {
            return NODE_STATUS[ input ]
        }
    }
]

var glyphicon = [
    function() {
        return function( input ) {
            if ( input === 200 ) {
                return "ok"
            } else {
                return "minus"
            }
        }
    }
]

var panel = [
    function() {
        return function( input ) {
            if ( input === 200 ) {
                return "success"
            } else {
                return "danger"
            }
        }
    }
]

exports.status = status
exports.glyphicon = glyphicon
exports.panel = panel

