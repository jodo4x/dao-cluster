"use strict";

var
    gulp = require( "gulp" ),
    plugins = require( "gulp-load-plugins")()

var
    source = {
        html: "./app/**/*.html",
        js: "./app/compo/**/*.js",
        css: "./app/scss/**/*.scss",
        font: "./app/vendor/fonts/**/*"
    },

    dist = "./dist",
    prod = process.env.NODE_ENV === "production"

gulp.task( "html", function() {
    return gulp.src( source.html )
    .pipe( plugins.if( prod,
        plugins.htmlmin( { collapseWhitespace: true } )
    ) )
    .pipe( gulp.dest( dist ) )
} )

gulp.task( "js", function() {
    return gulp.src( "./app/compo/main.js" )
    .pipe( plugins.browserify( {
        shim: {
            "angular-base64": {
                path: "./node_modules/angular-base64/angular-base64.js",
                exports: "base64"
            }
        }
    } ) )
    .pipe( plugins.if( prod, plugins.uglify() ) )
    .pipe( plugins.rename( "bundle.min.js" ) )
    .pipe( gulp.dest( dist + "/assets/js" ) )
} )

gulp.task( "css", function() {
    return gulp.src( "./app/scss/main.scss" )
    .pipe( plugins.sass( {
        outputStyle: prod ? "compressed" : "expanded"
    } ) )
    .pipe( plugins.rename( "bundle.min.css" ) )
    .pipe( gulp.dest( dist + "/assets/css" ) )
} )

gulp.task( "fonts", function() {
    return gulp.src( source.font )
    .pipe( gulp.dest( dist + "/assets/fonts" ) )
} )

gulp.task( "build", [ "html", "js", "css", "fonts" ] )

gulp.task( "serve", [ "build" ], function() {
    plugins.connect.server( {
        root: "./dist",
        port: 8000,
        livereload: prod ? false : true
    } )

    gulp.watch( [ 
        "./app/**/*.html",
        "./app/fonts/**/*"
    ] ).on( "error", plugins.connect.reload )
    gulp.watch( source.html, [ "html" ] )
    gulp.watch( source.js, [ "js" ] )
    gulp.watch( source.css, [ "css" ] )
} )

