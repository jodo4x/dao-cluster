FROM node:4.1.2

MAINTAINER yuwen.song@daocloud.io

RUN mkdir -p /root/daocluster-client \
    && npm install -g http-server
ADD . /root/daocluster-client
WORKDIR /root/daocluster-client 
EXPOSE 8000

CMD [ "bash" , "-c" , "sed -i \"s/192.168.1.92/${DEPLOYCONTROLLER_PORT_7433_TCP_ADDR}/g\" app/compo/serv.js; npm run serve"]

